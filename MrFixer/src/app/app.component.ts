import { Component } from '@angular/core';
import {AngularFire, FirebaseListObservable} from "angularfire2";
import {AuthServiceService} from "./Services/authservices/auth-service.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AuthServiceService]
})
export class AppComponent {
constructor(private af : AngularFire ){

  this.af.auth.subscribe(auth => console.log(auth));



  // const workers : FirebaseListObservable<any> = af.database.list('GeoLocation');
  //
  // workers.subscribe(
  //   val => console.log(val)
  //
  // )

}
}
//4200
