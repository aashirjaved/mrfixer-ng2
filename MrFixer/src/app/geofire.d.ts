declare namespace geofire {
  function GeoFire(firebaseRef: any): void;
}
declare module 'geofire' {
  function GeoFire(firebaseRef: any): void;
  export = GeoFire;
}
