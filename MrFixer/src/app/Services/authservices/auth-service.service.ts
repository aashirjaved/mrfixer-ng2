import {Injectable} from '@angular/core';
import {FirebaseAuthState, AngularFire, AuthMethods, AuthProviders} from "angularfire2";

@Injectable()
export class AuthServiceService {

  private _authState: FirebaseAuthState = null;

  constructor(public af: AngularFire) {

  }

  signUp(email: string, password: string) {
    var creds: any = {email: email, password: password};
    return this.af.auth.createUser(creds).catch((res) => {
      console.log(res)
      return res;
    }).then((err) => {
      console.log(err)
      return err;
    });
  }


  signIn(email: string, password: string) {
    var credentials: any = {
      email: email,
      password: password
    };
    return this.af.auth.login(credentials, {provider: AuthProviders.Password, method: AuthMethods.Password}
    ).catch((res) => {
      return res;
    }).then((err) => {
      return err
    });
  }
}
