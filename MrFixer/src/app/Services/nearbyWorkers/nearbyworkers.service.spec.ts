/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NearbyworkersService } from './nearbyworkers.service';

describe('NearbyworkersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NearbyworkersService]
    });
  });

  it('should ...', inject([NearbyworkersService], (service: NearbyworkersService) => {
    expect(service).toBeTruthy();
  }));
});
