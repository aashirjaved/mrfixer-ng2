import {Injectable, OnInit} from '@angular/core';
import {AngularFire, FirebaseListObservable} from "angularfire2";
import {isUndefined} from "util";

@Injectable()
export class NearbyworkersService {
  userposition: number;
  workersProfile = [];

  constructor(public af: AngularFire) {
    let lisfofworkers = [];
    var profile: WorkersInfo[] = [];
    console.log("Inside service constructor");
    const listofUID: WorkersLocation[] = [];
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(function (position) {
        var distance: number;
        const locations: FirebaseListObservable<any> = af.database.list('GeoLocation');
        locations.subscribe((res) => {
          res.forEach((entry) => {
            distance = getDistanceFromLatLonInKm(entry.l[0], entry.l[1], position.coords.latitude, position.coords.longitude);
            if (distance <= 5) {
              listofUID.push(
                {"id": entry.$key, "distance": distance}
              );
            }
            else {
              console.log("Worker out of serach radius")
            }
          });
          // for (let entry of listofUID) {
          const workers: FirebaseListObservable<any> = af.database.list('Retailers');
          workers.subscribe((res) => {
            res.forEach((entry) => {
              listofUID.forEach((data) => {
                if (data.id === entry.$key) {
                  lisfofworkers.push(entry);
                }
              })
            })

          });


        });
      });
      this.workersProfile = lisfofworkers;
    } else {
      console.log("Geolocation is not supported by this browser.");
    }


    console.log(this.workersProfile);
  }


  getWorkers() {
    return this.workersProfile;
  }

}
function deg2rad(deg: number) {
  return deg * (Math.PI / 180)
}
function getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2): number {
  var R = 6371; // Radius of the earth in km
  var dLat = deg2rad(lat2 - lat1);  // deg2rad below
  var dLon = deg2rad(lon2 - lon1);
  var a =
      Math.sin(dLat / 2) * Math.sin(dLat / 2) +
      Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
      Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
  var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  var d = R * c; // Distance in km
  return d;
}
type WorkersLocation = {
  id: number;
  distance: number;
}

type WorkersLocationType = {
  [key: string]: WorkersLocation;
}
type WorkersInfo = {
  name: string;
  distance: number;
  profession: string;
  phoneNo: string;

}

type WorkerInfoType = {
  [key: string]: WorkersLocation;
}
type dataofWorkers = {
  data: WorkersInfo;
  distance: string;
}
type ListOfWorkers = {
  [key: string]: dataofWorkers;
}
