import { Component, OnInit } from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthServiceService} from "../Services/authservices/auth-service.service";
import {ValidationService} from "../Services/validationservice/emailvalidation.service";

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  loginForm: FormGroup;
  userEmail: string = "";
  userPassword: string = "";

  constructor(private formBuilder: FormBuilder, private router: Router, private fireauth: AuthServiceService) {
    console.log("Inside signup")
  }

  ngOnInit() {
    this.buildForm();

  }
  buildForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, ValidationService.emailValidator]],
      password: ['', Validators.required]
    });
  }

  submit(em, pas) {
    console.log(`User clicked submit button` + em);
    this.fireauth.signUp(em, pas).then((res) => {
      if (res.uid !== undefined) {
        this.router.navigate(['maps']);
      }
      console.log(res)
    }).catch((response) => {
      console.log(response);

    });
  }

}
