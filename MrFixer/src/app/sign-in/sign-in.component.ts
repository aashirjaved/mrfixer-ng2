import {Component, OnInit, NgModule} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {AuthServiceService} from "../Services/authservices/auth-service.service";
import {ValidationService} from "../Services/validationservice/emailvalidation.service";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {
  loginForm: FormGroup;
  userEmail: string = "";
  userPassword: string = "";


  constructor(private formBuilder: FormBuilder, private router: Router, private fireauth: AuthServiceService) {

    }
  public signUp(){
    console.log("Sign Up clicked!!")
  }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, ValidationService.emailValidator]],
      password: ['', Validators.required]
    });
  }
  loading()
  {

  }

  submit(em, pas) {
    console.log(`User clicked submit button` + em);
    this.fireauth.signIn(em, pas).then((res) => {
      if (res.uid !== undefined) {
        this.router.navigate(['maps']);

      }
      console.log(res)
    }).catch((response) => {
      console.log(response);

    });
  }


}

export interface IUserLogin {
  email: string,
  password: string
}
