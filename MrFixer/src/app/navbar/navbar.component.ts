import {Component, OnInit, Inject} from '@angular/core';
import {Router} from "@angular/router";
import {DOCUMENT} from "@angular/platform-browser";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})

export class NavbarComponent implements OnInit {
  public currentURL : boolean  = false;

  constructor(@Inject(DOCUMENT) private document: any,private _router: Router ) {
    console.log(this._router.url);
    var url =  /[^/]*$/.exec(this.document.location.href)[0];
    console.log(url);
    if(url ==="login" || url ==="signup")
    {
      this.currentURL = true;
    }
    else
    {
      this.currentURL = false;
    }


  }

  ngOnInit() {
  }

}
