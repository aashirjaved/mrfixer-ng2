import {Component, OnInit} from '@angular/core';
import {NearbyworkersService} from '../Services/nearbyWorkers/nearbyworkers.service'
import {AuthServiceService} from "../Services/authservices/auth-service.service";
import {AngularFire} from "angularfire2";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

 public dataofworker;

  constructor(private nearbyusers: NearbyworkersService, private fireauth: AngularFire) {

  }


  ngOnInit() {
    var data = this.nearbyusers.getWorkers();
    this.dataofworker = data;
    console.log(this.dataofworker);
  }


}

