import { NgModule } from '@angular/core';
import { RouterModule, Routes, PreloadAllModules, NoPreloading } from '@angular/router';

import { MapsComponent } from './maps/maps.component';
import {SignInComponent} from './sign-in/sign-in.component'
import {SignUpComponent} from "./sign-up/sign-up.component";
import {HomeComponent} from "./home/home.component";

const app_routes: Routes = [
  //catch any unfound routes and redirect to home page
  // {
  //   path: '' , pathMatch:'full', redirectTo: '/maps'
  //  },
  { path: 'maps',  component: MapsComponent },
  { path: 'login',  component: SignInComponent },
  {path: 'signup',component: SignUpComponent},
  {path:'home',component: HomeComponent}
];

@NgModule({
  imports: [ RouterModule.forRoot(app_routes, { preloadingStrategy: PreloadAllModules }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {

}
