import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {AgmCoreModule} from 'angular2-google-maps/core';
import {NavbarComponent} from './navbar/navbar.component';
import{AlertModule} from 'ng2-bootstrap';
import {MapsComponent} from './maps/maps.component';
import {AppRoutingModule} from './app-routing.module';
import {firebaseConfig} from "../environments/firebase.config";
import {AngularFireModule, AuthProviders, AuthMethods} from "angularfire2";
import {AlertDirective} from './Directives/alert/alert.directive';
import {SignInComponent} from './sign-in/sign-in.component';
import {SignUpComponent} from './sign-up/sign-up.component';
import {ForgotPasswordComponent} from './forgot-password/forgot-password.component';
import {MaterialModule} from '@angular/material';
import {HomeComponent} from './home/home.component';
import {NearbyworkersService} from "./Services/nearbyWorkers/nearbyworkers.service";
import {CommonModule} from "@angular/common";

const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
};
@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    MapsComponent,
    AlertDirective,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    HomeComponent
  ],
  imports: [
    AlertModule,
    BrowserModule,
    CommonModule,
    FormsModule, ReactiveFormsModule,
    HttpModule,
    AppRoutingModule,
    MaterialModule.forRoot()
    ,
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig)
    ,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBnfvR2KoVNAjdYs-GpXjNPwqc9PoqVr4U'
    })
  ],
  providers: [NearbyworkersService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
