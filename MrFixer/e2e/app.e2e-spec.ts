import { MrFixerPage } from './app.po';

describe('mr-fixer App', function() {
  let page: MrFixerPage;

  beforeEach(() => {
    page = new MrFixerPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
